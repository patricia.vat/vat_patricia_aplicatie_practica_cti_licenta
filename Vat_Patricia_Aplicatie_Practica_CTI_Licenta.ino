 //#include <LiquidCrystal.h> //include libraria LCD
#include <DHT.h>    // include libraria dht
#include <Servo.h> //include libraria servo
#include<LiquidCrystal_I2C.h>

Servo myservo1; //creaza obiect servo pentru a putea fi controlat
Servo myservo2;
#define DHTTYPE DHT11 // defineste dhttype ca si dht11
#define DHTPIN 7//A0     // defineste dhtpin la A0
#define TRIG_PIN 12  // defineste trig_pin snezor ultrasonic la pin 12
#define ECHO_PIN 13  // defineste echo_pin senzor ultrasonic la pin 13
#define LED_PIN 9  // defineste led_pin la pin 9
#define DISTANTA_ACTIVARE 50  // defineste distanta de activare a led-ului
#define DRAPERIE_INCHISA 90  // defineste motorul pozitia 0
#define DRAPERIE_DESCHISA 0  //defineste motorul pozitia 180
#define Fan 11 //defineste pin-ul ventilatorului
#define PIN_FLACARA 5 //defineste pin-ul senzorului de flacara
#define LED 4 //5 //defineste pin-ul senzorului de flacara (led-ul cand sesiseaza flacara de pe senzor)
#define BUZZER 3 //defineste pin-ul de la buzzer cand sesizeaza flacara
#define USA_MAGNET 6 //defineste pin-ul de la senzorul de la usa magnetica
#define SOL A1 //defineste pin-ul de la senzorul pentru umiditatea solului
#define USCAT 300
#define UMED 700
#define APA 950
#define UMBRELA_INCHISA 45  // defineste motorul pozitia 0
#define UMBRELA_DESCHISA 0  //defineste motorul pozitia 180
#define LED_CAMERA 2 // defineste becul din camera la pin 2

long durata_ultrasonic = 0; 
int distanta_ultrasonic = 0;
int pin_motorsg=2;
int pozitie_jaluzele=0;
//int prezenta_flacara=HIGH;
int senzor_usa; // 0 e inchis si 1 deschis (pentru senzor usa magnetica)
int pin_motorsg2=8; //motorul servo definit pe pin-ul 8
int pozitie_umbrela=0; 
 int prezenta_apa=HIGH;
int apa_senzor=analogRead(0);


LiquidCrystal_I2C lcd(0x26, 16, 2);  // I2C address 0x3F, 16 column and 2 rows

//LiquidCrystal lcd(3, 4, 5, 6, 7, 8); // initializeaza libraria LCD la numarul de pini

DHT dht(DHTPIN, DHTTYPE);   // initializeaza senzorul DHT11

void setup () {
  Serial.begin (9600); // initializeaza portul serial
  dht.begin();  //initializeaza senzorul de temperatura si umiditate
  lcd.begin(16, 2);  //seteaza numarul de coloane si randuri de la LCD
  lcd.backlight(); // turn on backlight.

  pinMode(ECHO_PIN, INPUT);  // seteaza pin-ul arduino pe input mode(de la led)
  pinMode(TRIG_PIN, OUTPUT); // seteaza pin-ul arduino pe output mode(de la led)
  pinMode(LED_PIN, OUTPUT);  // seteaza pin-ul arduino pe output mode(de la led)
  pinMode(10,INPUT); // seteaza senzorul de lumina pe pin-ul 10
  pinMode(A2,OUTPUT); //seteaza senzorul de lumina la pin-ul analog A0, ?????, folosesc???
  pinMode(Fan,OUTPUT); //seteaza ventilatorul pe output mode(de la ventilator)
  pinMode(PIN_FLACARA,INPUT); //seteaza senzorul de flacara pe input mode
  pinMode(USA_MAGNET, INPUT_PULLUP); //seteaza senzorul magnetic pe input pullup mode
  myservo1.attach(pin_motorsg); 
  myservo2.attach(pin_motorsg2); 

}

//Functie ce citeste senzorul DHT11 si returneaza valoarea temperaturii si a umiditatii
void readDHT11(int& temperatura, int& umiditate){
  umiditate = dht.readHumidity();  // citeste umiditatea
  temperatura= dht.readTemperature();  // citeste temperatura si interpreteaza in grade celsius

   if ( (umiditate <10 || umiditate>90) && (temperatura<-10 || temperatura>40) ) { //testam daca senzorul merge si verificam daca umiditatea este peste 10% si temperatura peste -10 grade
    lcd.print("Failed to read from DHT sensor!"); //in caz contrat afisam pe LCD eroare
}
}

//functie ce printeaza pe LCD valorile senzorului DHT11
void printToLCD(int temperatura,int umiditate, int umiditate_sol){
  lcd.clear();
  lcd.setCursor(0, 0); //seteaza cursorul pe coloana 0 si randul 0
  lcd.print("UC:"); //printeaza umiditate
  lcd.print(umiditate); //valoarea umiditatii inregistrata
  lcd.print("%, ");
  lcd.print("TC:");  //printeaza umiditatea
  lcd.print(temperatura); //valoarea temperaturii inregistrata
  lcd.print((char)223);
  lcd.print("C");
  lcd.setCursor(0, 1); //seteaza cursorul pe coloana 0 randul 1
  lcd.print("US:");
  lcd.print(umiditate_sol); //valoarea umiditatii solului
  if(umiditate_sol>0 && umiditate_sol<USCAT)
  lcd.print(",SOL USCAT");
  else 
  if(umiditate_sol>USCAT && umiditate_sol<UMED)
  lcd.print(",SOL UMED");
  else
  if(umiditate_sol>UMED && umiditate_sol<APA)
  lcd.print(",APA");
}

//Functie pentru senzorul ultrasonic, calculare distanta pentru a putea aprinde led-ul
int aprindereLed(){
digitalWrite(TRIG_PIN, LOW); //senzorul primeste un puls LOW inainte ca sa verifice 
delayMicroseconds(2);
//senzorul primeste trigger de la un puls HIGH de 10 sau mai multe microsecunde
digitalWrite(TRIG_PIN, HIGH); 
delayMicroseconds(10);
digitalWrite(TRIG_PIN, LOW); 
durata_ultrasonic = pulseIn(ECHO_PIN, HIGH); //preia valoarea de la pin-ul echo
distanta_ultrasonic = durata_ultrasonic * 0.034 / 2; //converteste timpul in distanta
return distanta_ultrasonic; //returneaza distanta calculata
 

}

//Functie ce aprinde led-ul daca senzorul ultrasonic detecteaza miscare
void intrerupator_smart(int a,int b){
 if (aprindereLed() < DISTANTA_ACTIVARE) //testeaza daca senzorul ultrasonic detecteaza ceva in fata (50)
   digitalWrite(LED_PIN, HIGH); // se aprinde led-ul
  else
    digitalWrite(LED_PIN, LOW);  // se stinge led-ul

  
}

//Functie ce misca motorasul de pe pozitia 0 la 180 in functie de prezenta luminii
void jaluzele_smart(int c){
  int fotorezistor=digitalRead(10);
if(fotorezistor==0) //verifica daca senzorul de lumina detecteaza sursa de lumina 
  pozitie_jaluzele = DRAPERIE_DESCHISA; // daca da motorul se pune pe pozitia de 180 de grade
  else{
  pozitie_jaluzele = DRAPERIE_INCHISA;} //daca nu motorul se pune pe pozitia de 0 grade
myservo1.write(pozitie_jaluzele); 
delay(25);
}

//Functie ce activeaza ventilatorul in functie de temperatura ambientala 
void ventilator_smart(){
  int valoare;
  valoare=0; //viteza initiala a ventilatorului
  while(valoare<=255) //viteza cu care se misca elicea de la ventilator
  {
    analogWrite(Fan,valoare); //PWM
    valoare=valoare+5;
    delay(30);
  }
}

//Functie ce declanseaza un buzzer pasiv in momentul in care senzorul de flacara detecteaza foc
void senzor_incendiu(){
 int prezenta_flaca=digitalRead(PIN_FLACARA); //citeste data de la senzorul de flacara
  if(prezenta_flaca==1) //daca nu e prezenta flacara
  {
    noTone(BUZZER); //nu se aude alarma
    delay(1000);
  }
  else //daca e prezenta flacara
  {
   tone(BUZZER, 450); // se aude alarma
   delay(1000);
  }
}

//Functie ce declanseaza un buzzer pasiv in momentul in care usa principala e deschisa un anumit interval de timp
void usa_smart(){
 int senzor_usa=digitalRead(USA_MAGNET); //citeste data de la senzorul de la usa, daca e deschisa usa sau nu
  
  if(senzor_usa==HIGH){ //daca usa e deschisa, suna soneria
   tone(BUZZER, 400);
  }
  else{
    noTone(BUZZER); //daca usa e inchisa, nu suna soneria
  }
  delay(200);
}

void senzor_sol(){
  int umiditate_sol=analogRead(SOL);
if ( (umiditate_sol <0 ||umiditate_sol>900))
lcd.print("EROARE SOL");
}

//Functie ce misca motorasul de pe pozitia 0 la 180 in functie de prezenta apa
void terasa_smart(int c){
  int apa_senzor=analogRead(0);
if(apa_senzor>=200){ //verifica daca senzorul de apa detecteaza umiditate
  pozitie_umbrela = UMBRELA_DESCHISA; // daca da motorul se pune pe pozitia de 180 de grade si inchida umbrela
  delay(1000);
  }
  else{
  pozitie_umbrela = UMBRELA_INCHISA;} //daca nu motorul se pune pe pozitia de 0 grade
myservo2.write(pozitie_umbrela); 
delay(25);
}

//Functie ce aprinde led-ul din camera cand senzorul de lumina nu sesizeaza sursa de lumina
void lumina_camera(int z){
  int fotorezistor=digitalRead(10);
  if(fotorezistor ==1)
 digitalWrite(LED_CAMERA, HIGH); // se aprinde led-ul
  else
  digitalWrite(LED_CAMERA, LOW);  // se stinge led-ul
}


void loop() {
int temperatura, umiditate;
readDHT11(temperatura, umiditate); // apeleaza functia readDHT11 pentru a lua valorile senzorului

delay(500);
 int umiditate_sol=analogRead(SOL);

senzor_sol();//apeleaza functia senzorului de sol

printToLCD(temperatura, umiditate, umiditate_sol); //apeleaza functia printToLCD
 
intrerupator_smart(aprindereLed(),DISTANTA_ACTIVARE); //apeleaza functia intrerupator_smart

int fotorezistor=digitalRead(10);

jaluzele_smart(fotorezistor);  // apeleaza functia jaluzele_smart
if(temperatura>23){ //verifica daca temperatura e mai mare decat x si declanseaza ventilatorul
ventilator_smart(); //apeleaza functia ventilator smart
}
lumina_camera(fotorezistor);
senzor_incendiu(); //apeleaza functia senzorului de flacara
usa_smart(); //apeleaza functia senzor magnetic usa
delay(200);
myservo2.attach(pin_motorsg2); 
terasa_smart(apa_senzor);
delay(1000);

}
